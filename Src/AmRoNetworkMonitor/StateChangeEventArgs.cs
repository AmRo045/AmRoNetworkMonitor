﻿using System;

namespace AmRoNetworkMonitor
{
    public class StateChangeEventArgs : EventArgs
    {
        public bool IsAvailable { get; internal set; }
    }
}