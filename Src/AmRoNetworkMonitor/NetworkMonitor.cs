﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using System.Timers;

namespace AmRoNetworkMonitor
{
    public static class NetworkMonitor
    {
        #region Fields

        /// <summary>
        /// Timer interval - Default 5 second
        /// </summary>
        private const int TimerInterval = 5000;

        /// <summary>
        /// for checking internet connection
        /// </summary>
        private const string Google204Generator = "http://clients3.google.com/generate_204";

        private static Timer _networkTimer;

        #endregion

        #region Properties

        /// <summary>
        /// Get current network state
        /// </summary>
        public static bool CurrentState { get; set; }

        #endregion

        #region Events

        public static event EventHandler<StateChangeEventArgs> StateChanged;

        private static void OnStateChanged(bool state)
        {
            CurrentState = state;
            StateChanged?.Invoke(null, new StateChangeEventArgs {IsAvailable = state});
        }

        #endregion

        #region Methods

        /// <summary>
        /// Start network monitoring
        /// </summary>
        public static void StartMonitor()
        {
            _networkTimer = new Timer { Interval = TimerInterval };
            _networkTimer.Elapsed += NetworkMonitorElapsed;
            NetworkMonitorElapsed(null, null);
            _networkTimer.Enabled = true;
        }

        /// <summary>
        /// Stop network monitoring
        /// </summary>
        public static void StopMonitor()
        {
            _networkTimer.Enabled = false;
            _networkTimer.Elapsed -= NetworkMonitorElapsed;
            _networkTimer.Dispose();
        }

        /// <summary>
        /// Timer elapsed handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static async void NetworkMonitorElapsed(object sender, ElapsedEventArgs e)
        {
            if (NetworkInterface.GetIsNetworkAvailable() && await InternetIsAvaiableAsync())
                OnStateChanged(true);
            else
                OnStateChanged(false);
        }

        /// <summary>
        /// Check internet access
        /// </summary>
        /// <returns></returns>
        private static async Task<bool> InternetIsAvaiableAsync()
        {
            try
            {
                using (var client = new WebClient())
                    await client.OpenReadTaskAsync(Google204Generator);
                return true;
            }
            catch
            {
                return false;
            }
        }

        #endregion
    }
}
